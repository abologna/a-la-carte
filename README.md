# À la carte

Mix and match C, Rust and Go.

## Description

This repository shows how it's possible to make libraries implemented
in either Rust or Go consumable by the other language, as well as C.

It's not intended to be a production-grade implementation, but rather
a showcase that hopefully covers enough non-trivial scenarios to
demonstrate the feasibilty of the idea.

## Repository structure

It's basically a bunch of mini-projects stored in a single repository
for convenience; each one can be consumed on its own, although there
are dependencies between some.

Specifically:

* `rust/native` and `go/native`: à la carte, implemented in Rust and
  Go respectively;

* `c/rust` and `c/go`: C bindings for à la carte, calling to the
  Rust and Go implementation respectively;

* `rust/go` and `go/rust`: Rust bindings for the Go implementation
  of à la carte and vice versa.

## Build system

Each sub-project has its own `Makefile` which exposes a number of
standard targets:

* `build`, `clean`, `test`: pretty self-explanatory;

* `fmt`, `verify-fmt`, `vet`: automatically format code, verify it
  is formatted properly, and try to spot issues;

* `run-examples`: run all example programs.

Some targets are no-op for certain sub-projects because they don't
make sense in that context.

The top-level `Makefile` provides a convenient way to build and test
all sub-projects at once.

## License

À la carte is distributed under the terms of the MIT License. See
[`LICENSE`](LICENSE) for details.
