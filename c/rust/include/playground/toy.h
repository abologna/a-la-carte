/* À la carte
 *
 * Copyright (C) 2019-2020 Red Hat, Inc.
 *
 * This software is distributed under the terms of the MIT License.
 * See the LICENSE file in the top level directory for details.
 */

#pragma once

typedef enum {
    ALC_PLAYGROUND_TOY_ERROR_CALLBACK_FAILED = 0,
} AlcPlaygroundToyError;

typedef struct _AlcPlaygroundToy AlcPlaygroundToy;

typedef bool (*AlcPlaygroundToyCallback)(const AlcPlaygroundToy *toy,
                                         const char *ext,
                                         void *data);
typedef void (*AlcPlaygroundToyCallbackDataFree)(void *data);

AlcPlaygroundToy *alc_playground_toy_new(const char *base,
                                         AlcPlaygroundToyCallback filter,
                                         void *data,
                                         AlcPlaygroundToyCallbackDataFree data_free);
void alc_playground_toy_free(AlcPlaygroundToy *toy);

char *alc_playground_toy_get_base(const AlcPlaygroundToy *toy);

char *alc_playground_toy_run(const AlcPlaygroundToy *toy,
                             const char *ext,
                             AlcError **error);

ALC_DEFINE_AUTOPTR_FUNC(AlcPlaygroundToy,
                        alc_playground_toy_free);
