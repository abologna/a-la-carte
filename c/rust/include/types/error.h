/* À la carte
 *
 * Copyright (C) 2019-2020 Red Hat, Inc.
 *
 * This software is distributed under the terms of the MIT License.
 * See the LICENSE file in the top level directory for details.
 */

#pragma once

typedef enum {
    ALC_ERROR_DOMAIN_PLAYGROUND_TOY_ERROR = 0,
} AlcErrorDomain;

typedef struct _AlcError AlcError;

void alc_error_free(AlcError *error);

AlcErrorDomain alc_error_get_domain(const AlcError *error);
unsigned int alc_error_get_code(const AlcError *error);
char *alc_error_get_message(const AlcError *error);

ALC_DEFINE_AUTOPTR_FUNC(AlcError,
                        alc_error_free);
