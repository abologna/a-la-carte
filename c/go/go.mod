module gitlab.com/abologna/a-la-carte/c/go

go 1.12

require gitlab.com/abologna/a-la-carte/go/native v0.0.0

replace gitlab.com/abologna/a-la-carte/go/native => ../../go/native
