// À la carte
//
// Copyright (C) 2019-2020 Red Hat, Inc.
//
// This software is distributed under the terms of the MIT License.
// See the LICENSE file in the top level directory for details.

package main

/*
#include "libalc_c_go.h"
#include "alc.h"
#include "private.h"

AlcPlaygroundToy*
alc_playground_toy_new(const char *base,
                       AlcPlaygroundToyCallback filter,
                       void *data,
                       AlcPlaygroundToyCallbackDataFree dataFree)
{
    return playground_toy_wrap(playground_toy_new((char *) base, filter, data, dataFree));
}

void
alc_playground_toy_free(AlcPlaygroundToy *toy)
{
    if (toy == NULL) return;
    playground_toy_free(toy->goPtr);
    free(toy);
}

char*
alc_playground_toy_get_base(const AlcPlaygroundToy *toy)
{
    assert(toy != NULL);
    return playground_toy_get_base(toy->goPtr);
}

char*
alc_playground_toy_run(const AlcPlaygroundToy *toy,
                       const char *ext,
                       AlcError **error)
{
    unsigned int goPtr;
    char *ret;

    assert(toy != NULL);
    assert(error != NULL);

    ret = playground_toy_run(toy->goPtr, (char *) ext, &goPtr);

    if (goPtr) {
        *error = error_wrap(goPtr);
    } else {
        *error = NULL;
    }

    return ret;
}
*/
import "C"
