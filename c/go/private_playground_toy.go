// À la carte
//
// Copyright (C) 2019-2020 Red Hat, Inc.
//
// This software is distributed under the terms of the MIT License.
// See the LICENSE file in the top level directory for details.

package main

/*
#include "alc.h"
#include "private.h"

AlcPlaygroundToy*
playground_toy_wrap(unsigned int goPtr)
{
    AlcPlaygroundToy *self = malloc(sizeof(AlcPlaygroundToy));
    self->goPtr = goPtr;
    return self;
}

bool
playground_toy_callback_call(AlcPlaygroundToyCallback callback,
                             const AlcPlaygroundToy *toy,
                             const char *ext,
                             void *data)
{
    return callback(toy, ext, data);
}

void
playground_toy_callback_data_free_call(AlcPlaygroundToyCallbackDataFree dataFree,
                                       void *data)
{
    dataFree(data);
}
*/
import "C"
