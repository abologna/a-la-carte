// À la carte
//
// Copyright (C) 2019-2020 Red Hat, Inc.
//
// This software is distributed under the terms of the MIT License.
// See the LICENSE file in the top level directory for details.

package types

import (
	"gitlab.com/abologna/a-la-carte/go/native/pkg/playground"
)

type ErrorDomain uint

const (
	PlaygroundToyError ErrorDomain = iota
)

type Error struct {
	native error
}

func NewError(err error) *Error {
	return &Error{native: err}
}

func (self *Error) Domain() ErrorDomain {
	var domain ErrorDomain

	switch self.native.(type) {
	case playground.ToyError:
		domain = ErrorDomain(PlaygroundToyError)
	}

	return domain
}

func (self *Error) Code() uint {
	var code uint

	switch self.native.(type) {
	case playground.ToyError:
		code = uint(self.native.(playground.ToyError))
	}

	return code
}

func (self *Error) Message() string {
	return self.native.Error()
}
