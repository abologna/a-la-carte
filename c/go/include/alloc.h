/* À la carte
 *
 * Copyright (C) 2019-2020 Red Hat, Inc.
 *
 * This software is distributed under the terms of the MIT License.
 * See the LICENSE file in the top level directory for details.
 */

#pragma once

#define ALC_FREE(_ptr) \
    do { \
        if (_ptr) \
            free((void *) _ptr); \
        _ptr = NULL; \
    } while(false)

static inline void alc_autofree_helper(void *ptr)
{
    if (*(void **)ptr)
        free(*(void **)ptr);
    *(void **)ptr = NULL;
}

#define ALC_AUTOFREE(_type) \
    __attribute__((cleanup(alc_autofree_helper))) _type

#define ALC_AUTOPTR_FUNC_NAME(_type) _type##AlcAutoPtrFunc

#define ALC_DEFINE_AUTOPTR_FUNC(_type, _func) \
    static inline void ALC_AUTOPTR_FUNC_NAME(_type)(_type **_ptr) \
    { \
        if (*_ptr) \
            (_func)(*_ptr); \
        *_ptr = NULL; \
    }

#define ALC_AUTOPTR(_type) \
    __attribute__((cleanup(ALC_AUTOPTR_FUNC_NAME(_type)))) _type *
