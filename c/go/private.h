/* À la carte
 *
 * Copyright (C) 2019-2020 Red Hat, Inc.
 *
 * This software is distributed under the terms of the MIT License.
 * See the LICENSE file in the top level directory for details.
 */

#pragma once

#include <assert.h>

#include "private_include/playground/all.h"
#include "private_include/types/all.h"
