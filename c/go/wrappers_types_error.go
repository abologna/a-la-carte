// À la carte
//
// Copyright (C) 2019-2020 Red Hat, Inc.
//
// This software is distributed under the terms of the MIT License.
// See the LICENSE file in the top level directory for details.

package main

/*
#include "libalc_c_go.h"
#include "alc.h"
#include "private.h"

void
alc_error_free(AlcError *error)
{
    if (error == NULL) return;
    error_free(error->goPtr);
    free(error);
}

AlcErrorDomain
alc_error_get_domain(const AlcError *error)
{
    assert(error != NULL);
    return error_get_domain(error->goPtr);
}

unsigned int
alc_error_get_code(const AlcError *error)
{
    assert(error != NULL);
    return error_get_code(error->goPtr);
}

char *
alc_error_get_message(const AlcError *error)
{
    assert(error != NULL);
    return error_get_message(error->goPtr);
}
*/
import "C"
