// À la carte
//
// Copyright (C) 2019-2020 Red Hat, Inc.
//
// This software is distributed under the terms of the MIT License.
// See the LICENSE file in the top level directory for details.

#pragma once

#include <alc.h>

bool playground_toy_callback_trampoline_c(const AlcPlaygroundToy *toy,
                                          const char *ext,
                                          void *data);
int playground_toy_callback_trampoline_go(AlcPlaygroundToy *toy,
                                          char *ext,
                                          int callback);
