// À la carte
//
// Copyright (C) 2019-2020 Red Hat, Inc.
//
// This software is distributed under the terms of the MIT License.
// See the LICENSE file in the top level directory for details.

use crate::c;
use std::error;
use std::ffi::CStr;
use std::ffi::CString;
use std::fmt;
use std::os::raw::c_char;
use std::os::raw::c_void;
use std::ptr::null_mut;

#[derive(Copy, Clone, Debug)]
pub enum ToyErrorCode {
    CallbackFailed,
}

#[derive(Debug)]
pub struct ToyError {
    ptr: *mut c::AlcError,
}

impl ToyError {
    fn from(c_err: *mut c::AlcError) -> Self {
        if c_err.is_null() {
            panic!("c_err: Invalid native pointer");
        }

        Self { ptr: c_err }
    }

    pub fn code(&self) -> ToyErrorCode {
        let rust_code;

        unsafe {
            rust_code = match c::alc_error_get_code(self.ptr) {
                c::ALC_PLAYGROUND_TOY_ERROR_CALLBACK_FAILED => ToyErrorCode::CallbackFailed,
                _ => panic!("Unrecognized AlcPlaygroundToyError value"),
            };
        }

        rust_code
    }

    pub fn message(&self) -> String {
        let rust_msg;

        unsafe {
            let c_msg = c::alc_error_get_message(self.ptr);
            rust_msg = String::from(CStr::from_ptr(c_msg).to_str().unwrap());
        }

        rust_msg
    }
}

impl Drop for ToyError {
    fn drop(&mut self) {
        unsafe {
            c::alc_error_free(self.ptr);
        }
    }
}

impl fmt::Display for ToyError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.message())
    }
}

impl error::Error for ToyError {}

pub struct Toy {
    ptr: *mut c::AlcPlaygroundToy,
}

extern "C" fn invoke_callback<F>(
    c_toy: *const c::AlcPlaygroundToy,
    c_ext: *const c_char,
    c_data: *mut c_void,
) -> bool
where
    F: Fn(&Toy, &str) -> bool + 'static,
{
    // Build a new Rust object to wrap the actual C object. For our
    // purposes it doesn't matter that this Rust object is not the same
    // that was created previously, because it's just a shim and all
    // operations are actually performed on the C object anyway
    let mut rust_toy = Toy {
        ptr: c_toy as *mut c::AlcPlaygroundToy,
    };
    let rust_ext: &str;
    let rust_filter: &mut F;

    unsafe {
        rust_ext = CStr::from_ptr(c_ext).to_str().unwrap();
        let c_filter = c_data as *mut F;
        rust_filter = &mut *c_filter;
    }

    let ret = rust_filter(&rust_toy, rust_ext);

    // If we don't reset the C pointer here, when rust_toy's drop()
    // method will run at the end of the function it will free() the
    // C object, which is not what we want
    rust_toy.ptr = null_mut();

    ret
}

extern "C" fn free_callback<F>(c_data: *mut c_void)
where
    F: Fn(&Toy, &str) -> bool + 'static,
{
    unsafe {
        let c_filter = c_data as *mut F;
        let _ = Box::from_raw(c_filter);
    }
}

impl Toy {
    pub fn new<F>(rust_base: &str, rust_filter: F) -> Self
    where
        F: Fn(&Self, &str) -> bool + 'static,
    {
        let c_ptr;
        let c_filter = Box::into_raw(Box::new(rust_filter));
        let c_base = CString::new(rust_base)
            .expect("rust_base: Can't convert")
            .into_raw();

        unsafe {
            // C code can't invoke the Rust callback provided by the user
            // directly, so we use a special generic C-compatible function,
            // invoke_callback(), to jump back into Rust from C. As for the
            // actual Rust callback, we take advantage of the C callback's
            // user_data argument to pass it around
            c_ptr = c::alc_playground_toy_new(
                c_base,
                Some(invoke_callback::<F>),
                c_filter as *mut c_void,
                Some(free_callback::<F>),
            );

            let _ = CString::from_raw(c_base);
        }

        Self { ptr: c_ptr }
    }

    pub fn base(&self) -> String {
        let rust_base;

        unsafe {
            let c_base = c::alc_playground_toy_get_base(self.ptr);

            rust_base = String::from(CStr::from_ptr(c_base).to_str().unwrap());
        }

        rust_base
    }

    pub fn run(&self, rust_ext: &str) -> Result<String, ToyError> {
        let c_toy = self.ptr;
        let c_ext = CString::new(rust_ext)
            .expect("rust_ext: Can't convert")
            .into_raw();
        let mut c_err: *mut c::AlcError = null_mut();
        let c_res;

        unsafe {
            c_res = c::alc_playground_toy_run(c_toy, c_ext, &mut c_err);

            let _ = CString::from_raw(c_ext);
        }

        if c_err.is_null() {
            unsafe { Ok(String::from(CStr::from_ptr(c_res).to_str().unwrap())) }
        } else {
            Err(ToyError::from(c_err))
        }
    }
}

impl Drop for Toy {
    fn drop(&mut self) {
        unsafe {
            c::alc_playground_toy_free(self.ptr);
        }
    }
}
